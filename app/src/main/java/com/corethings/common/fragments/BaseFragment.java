/*
 * Copyright (c) 2012-2015 Arne Schwabe
 * Distributed under the GNU GPL v2 with additional terms. For full terms see the file doc/LICENSE.txt
 */

package com.corethings.common.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;

import com.corethings.R;

public abstract class BaseFragment extends Fragment {

    public static boolean sDisableFragmentAnimations = false;

    protected void showDialog(String message, String done, String cancel, DialogClickListener listener) {
        AlertDialogFragment fragment = AlertDialogFragment.newInstance(message, done, cancel, listener);
        fragment.show(getFragmentManager(), "Dialog");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);


    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    protected <T> void changeActivity(Class<T> cls, Bundle data) {
        Intent resultIntent = new Intent(getActivity(), cls);
        if (data != null)
            resultIntent.putExtras(data);
        startActivity(resultIntent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    protected <T> void changeActivity(Class<T> cls) {
        Intent resultIntent = new Intent(getActivity(), cls);
        startActivity(resultIntent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        getActivity().finish();
    }

    protected <T> void changeActivity(Class<T> cls, boolean is_finish) {
        Intent resultIntent = new Intent(getActivity(), cls);
        startActivity(resultIntent);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public android.app.ActionBar getActionBar() {
        return getActivity().getActionBar();
    }

}

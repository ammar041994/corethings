package com.corethings.common.fragments;

/**
 * Created by Noman on 3/14/2015.
 */
public interface DialogClickListener {
    void onPositiveButtonClick();
}

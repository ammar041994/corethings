package com.corethings.common.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.corethings.R;
import com.corethings.common.logger.Log;
import com.corethings.common.logger.LogWrapper;


public class BaseActionBarActivity extends AppCompatActivity {
    private static final String TAG = "Base Activity";

    protected FragmentManager.OnBackStackChangedListener mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            setActionBarArrowDependingOnFragmentsBackStack();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
        initializeLogging();
    }

    private void setActionBarArrowDependingOnFragmentsBackStack() {
        int backStackEntryCount =
                getFragmentManager().getBackStackEntryCount();
        getActionBar().setDisplayHomeAsUpEnabled(backStackEntryCount > 0);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            popFragmentIfStackExist();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void popFragmentIfStackExist() {
        int backStackEntryCount =
                getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount > 0)
            getSupportFragmentManager().popBackStack();
    }

    /**
     * Set up targets to receive log data
     */
    public void initializeLogging() {
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        // Wraps Android's native log framework
        LogWrapper logWrapper = new LogWrapper();
        Log.setLogNode(logWrapper);
        Log.i(TAG, "Ready");
    }

    protected <T> void changeActivity(Intent intent) {
        overridePendingTransition(R.anim.enter, R.anim.exit);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected <T> void changeActivity(Class<T> cls, boolean is_finish) {
        Intent resultIntent = new Intent(this, cls);
        startActivity(resultIntent);
        this.overridePendingTransition(R.anim.enter, R.anim.exit);

    }
}

package com.corethings.common.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;

import com.corethings.R;


/**
 * Created by Anas on 1/26/2015.
 */
public class TransparentProgressDialog extends ProgressDialog {


    public TransparentProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams windowAttrs = getWindow().getAttributes();
        windowAttrs.gravity = Gravity.CENTER;
        getWindow().setAttributes(windowAttrs);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

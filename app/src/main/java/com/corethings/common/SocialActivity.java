package com.corethings.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.corethings.models.FacebookModel;
import com.corethings.models.MediaModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Media;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.MediaService;
import com.twitter.sdk.android.core.services.StatusesService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit.mime.TypedFile;

/**
 * Created by ammar on 6/24/16.
 */
public class SocialActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {
    String profileUrl = "https://api.linkedin.com/v1/people/~";
    String shareUrl = "https://api.linkedin.com/v1/people/~/shares";
    private Gson gson = new Gson();
    private CallbackManager mCallbackManager;
    public GoogleApiClient mGoogleApiClient;
    int GOOGLE_SIGN_IN = 4000, LINKEDIN_SIGN_IN;
    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInflow = true;
    private boolean mSignInClicked = false;
    private boolean twitterShare = false, facebookShare = false, linkedinShare = false, googleShare = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fbInitialization();
        initGooglePlus();

    }

    public void initGooglePlus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity ​*/, this /*​ OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



    }

    public void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("GoogleSignIn", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    public void initLinkedin() {
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Authentication was successful.  You can now do
                // other calls with the SDK.
                Log.e("Linkedin Auth", "Success");
                getLinkedinUserProfile();
            }

            @Override
            public void onAuthError(LIAuthError error) {
                // Handle authentication errors
                Log.e("Linkedin Error", error.toString());
            }
        }, true);


    }

    public void getLinkedinUserProfile() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, profileUrl, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                Log.e("apiResponse", gson.toJson(apiResponse));
                Log.e("apiResponse", apiResponse.getResponseDataAsJson().toString());

            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!
            }
        });
    }

    public boolean shareOnLinkedin(String comment, String link) throws JSONException {
        JSONObject post = new JSONObject();
        JSONObject content = new JSONObject();
        content.put("title", "");
        content.put("description", "");
        if (link.equalsIgnoreCase("")) {
            content.put("submitted-url", "http://statusgeek.co");
        } else {
            content.put("submitted-url", link);
        }
        //content.put("submitted-image-url", "");
        JSONObject visibility = new JSONObject();
        visibility.put("code", "anyone");

        post.put("comment", comment);
        post.put("content", content);
        post.put("visibility", visibility);
        Log.e("Post", post.toString());
        String payload = post.toString();

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());

        apiHelper.postRequest(this, shareUrl, payload, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                Log.e("apiResponse", gson.toJson(apiResponse));
                linkedinShare = true;
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making POST request!
                liApiError.printStackTrace();
                linkedinShare = false;
            }
        });

        return linkedinShare;
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Add this line to your existing onActivityResult() method
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);

        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            //
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    public void fbInitialization() {

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");

                        String token = loginResult.getAccessToken().getToken();
                        Utilities.saveData(getActivity(), "access_token", token);


                        final GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("Login" +
                                                "Activity", response.toString());

                                        try {
                                            Gson gson = new Gson();

                                            JSONObject picture = object.getJSONObject("picture");
                                            JSONObject data = picture.getJSONObject("data");
                                            Utilities.saveData(getActivity(), "profileImage", data.getString("url"));
                                            FacebookModel.setInstance(getActivity(), gson.fromJson(object.toString(), FacebookModel.class));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                    }
                });
    }


    public void signInFB() {
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email", "user_photos", "user_tagged_places"));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            // Already resolving
            return;
        }

        // If the sign in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        if (mSignInClicked || mAutoStartSignInflow) {
            mAutoStartSignInflow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign in, please try again later."
//            if (!BaseGameUtils.resolveConnectionFailure(this,
//                    mGoogleApiClient, connectionResult,
//                    RC_SIGN_IN, R.string.signin_other_error)) {
//                mResolvingConnectionFailure = false;
//            }
        }

    }

    public boolean updateStatus(String message, String place, String link) {
        Bundle params = new Bundle();
        params.putString("message", message);
        params.putString("link", link);
        if (!place.equalsIgnoreCase(""))
            params.putString("place", place);
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/feed",
                params,
                HttpMethod.POST,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        facebookShare = true;
                        //Utilities.toast(getActivity(), "Posted");
                    }
                }
        ).executeAsync();
        return facebookShare;
    }

    public boolean updateStatusWithPhoto(final ArrayList<MediaModel> list, String message, String place, String link) {
        final GraphRequestBatch batch = new GraphRequestBatch();
        for (int i = 0; i < list.size(); i++) {
            Bundle params = new Bundle();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            list.get(i).getImageLarge().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            params.putByteArray("source", byteArray);
            params.putString("message", message);
            params.putString("link", link);
            if (!place.equalsIgnoreCase(""))
                params.putString("place", place);
            GraphRequest request = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/photos",
                    params,
                    HttpMethod.POST,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            if (batch.size() == list.size()) {
                                facebookShare = true;
                                //Utilities.toast(getActivity(), "Posted");
                            }

                        }
                    }
            );

            batch.add(request);
        }

        batch.executeAsync();
        return facebookShare;
    }

    public void getPublishActionsPermission() {

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + FacebookModel.getInstance(getActivity()).getId() + "/permissions",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        String permission, status;

                        JSONObject responseObj = response.getJSONObject();
                        try {
                            JSONArray data = responseObj.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerData = data.getJSONObject(i);
                                permission = innerData.getString("permission");
                                status = innerData.getString("status");

                                if (permission.equalsIgnoreCase("publish_actions") && status.equalsIgnoreCase("granted")) {
                                    Utilities.saveBoolean(getActivity(), "publish_actions", true);
                                }
                            }

                            if (!Utilities.getSavedBoolean(getActivity(), "publish_actions")) {
                                LoginManager.getInstance().logInWithPublishPermissions(
                                        getActivity(),
                                        Arrays.asList("publish_actions"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("data", responseObj.toString());
            /* handle the result */
                    }
                }

        ).

                executeAsync();

    }


    public Bitmap getLargeBitmap(Uri uri) {
        File image = new File(uri.getPath());
        Bitmap bm = BitmapFactory.decodeFile(image.getPath());
        if (bm != null) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            width = width / 2;
            height = height / 2;
            Log.e("width", "" + width);
            Log.e("height", "" + height);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                    bm, width, height, false);
            return resizedBitmap;
        } else {
            return bm;
        }


        //getRoundedCornerBitmap(resizedBitmap, 100);

    }

    public Bitmap getLargeBitmapIncoming(Uri uri) throws IOException {
        File image = new File(uri.getPath());
        Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
        int width = bm.getWidth();
        int height = bm.getHeight();
        width = width / 4;
        height = height / 4;
        Log.e("width", "" + width);
        Log.e("height", "" + height);
        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                bm, width, height, false);
        return resizedBitmap;//getRoundedCornerBitmap(resizedBitmap, 100);

    }

    public Bitmap getResizedBitmap(Bitmap bm) {
        if (bm != null) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            if (width > 1500 || height > 1500) {
                width = width / 6;
                height = height / 6;
            } else if (width > 1000 || height > 1000) {
                width = width / 5;
                height = height / 5;
            } else if (width > 600 || height > 600) {
                width = width / 4;
                height = height / 4;
            } else if (width > 400 || height > 400) {
                width = width / 3;
                height = height / 3;
            }
            Log.e("width", "" + width);
            Log.e("height", "" + height);
            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                    bm, width, height, false);
            return resizedBitmap;
        }


        return bm;
        //getRoundedCornerBitmap(resizedBitmap, 100);

    }


    public boolean updateStatusTwitterWithImages(final ArrayList<MediaModel> list, final String status, final double latitude, final double longitude) {
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        final StatusesService statusesService = twitterApiClient.getStatusesService();
        final ArrayList<Long> mediaIds = new ArrayList<>();
        final MediaService mediaService = twitterApiClient.getMediaService();
        for (int i = 0; i < list.size(); i++) {
            File photo = new File(list.get(i).getUriImage().getPath());
            TypedFile typedFile = new TypedFile("application/octet-stream", photo);
            mediaService.upload(typedFile, null, null, new Callback<Media>() {
                @Override
                public void success(Result<Media> result) {
                    Log.e("result", "Media Result");
                    mediaIds.add(result.data.mediaId);
                    String ids = "";
                    if (list.size() == mediaIds.size()) {

                        for (int i = 0; i < mediaIds.size(); i++) {
                            if (i == mediaIds.size()) {
                                ids = ids + mediaIds.get(i);
                            } else {
                                ids = ids + mediaIds.get(i) + ",";
                            }
                        }
                        statusesService.update(
                                status, // Status Message
                                null, //  reply to old status
                                false, // possiblySensitive
                                latitude,//latitude
                                longitude,//longitude
                                null,//placeId
                                true,//displayCoordinates
                                true,//trimUser
                                ids,//mediaIds
                                new Callback<Tweet>() {
                                    @Override
                                    public void success(Result<Tweet> result) {
                                        Log.e("Tweet", "Succes");
                                    }

                                    @Override
                                    public void failure(TwitterException exception) {
                                        exception.printStackTrace();
                                    }
                                });
                    }
                    twitterShare = true;
                }

                @Override
                public void failure(TwitterException exception) {
                    exception.printStackTrace();
                    twitterShare = false;
                }
            });
        }

        return twitterShare;
    }


    public boolean updateStatusTwitter(String status, double latitude, double longitude) {
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        final StatusesService statusesService = twitterApiClient.getStatusesService();


        statusesService.update(
                status, // Status Message
                null, //  reply to old status
                false, // possiblySensitive
                latitude,//latitude
                longitude,//longitude
                null,//placeId
                false,//displayCoordinates
                false,//trimUser
                //ids,//mediaIds
                new Callback<Tweet>() {
                    @Override
                    public void success(Result<Tweet> result) {
                        Log.e("Tweet", "Success");
                        twitterShare = true;
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        exception.printStackTrace();
                        twitterShare = false;
                    }
                });
        return twitterShare;
    }


    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
            int contentViewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getActivity());

            if (heightDiff <= contentViewTop) {
                onHideKeyboard();

                Intent intent = new Intent("KeyboardWillHide");
                broadcastManager.sendBroadcast(intent);
            } else {
                int keyboardHeight = heightDiff - contentViewTop;
                onShowKeyboard(keyboardHeight);

                Intent intent = new Intent("KeyboardWillShow");
                intent.putExtra("KeyboardHeight", keyboardHeight);
                broadcastManager.sendBroadcast(intent);
            }
        }
    };

    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;

    protected void onShowKeyboard(int keyboardHeight) {
    }

    protected void onHideKeyboard() {
    }

    protected void attachKeyboardListeners(int layout) {
        if (keyboardListenersAttached) {
            return;
        }
        rootLayout = (ViewGroup) findViewById(layout);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        keyboardListenersAttached = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
        }
    }

    public FragmentActivity getActivity() {
        return SocialActivity.this;
    }
}

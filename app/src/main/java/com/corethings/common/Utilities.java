/*
 * Copyright (c) 2012-2015 Arne Schwabe
 * Distributed under the GNU GPL v2 with additional terms. For full terms see the file doc/LICENSE.txt
 */

package com.corethings.common;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Utilities {


    public static final String USER_OBJECT = "FACEBOOK_OBJECT";
    public static <T> void printJSON(String TAG, Object object, Class<T> classOfT) {
        Gson gson = new Gson();
        String jsonStr = gson.toJson(object, classOfT);
        Log.i(TAG, jsonStr);
    }

    public static <T> String getJSON(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static <T> String getJSON(Object object, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.toJson(object, classOfT);
    }

    public static <T> void printJSON(String TAG, Object object) {
        Gson gson = new Gson();
        String jsonStr = gson.toJson(object);
        Log.i(TAG, jsonStr);
    }

    public static <T> T getObjectFromGSON(String object, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(object, classOfT);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static <T> ArrayList<T> getArrayListFromGSON(String object) {
        try {
            InputStream stream = new ByteArrayInputStream(object.getBytes(StandardCharsets.UTF_8));
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            Gson gson = new Gson();
            return gson.fromJson(br, new TypeToken<ArrayList<T>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void saveData(Context context, String key, String value) {
        try {
            SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                    Context.MODE_PRIVATE);
            sp.edit().putString(key, value).apply();
        } catch (Exception e) {

        }
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).apply();
    }

    public static void saveInt(Context context, String key, int value) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).apply();
    }

    public static void saveLong(Context context, String key, long value) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        sp.edit().putLong(key, value).apply();
    }


    public static long getSavedLong(Context context, String key) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),


                Context.MODE_PRIVATE);
        return sp.getLong(key, -1);
    }

    public static int getSavedInt(Context context, String key) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),


                Context.MODE_PRIVATE);
        return sp.getInt(key, -1);
    }

    public static String getSaveData(Context context, String key) {
        try {
            SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                    Context.MODE_PRIVATE);
            return sp.getString(key, null);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean getSavedBoolean(Context context, String key) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    public static boolean getSavedBooleanDefaultTrue(Context context, String key) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, true);
    }

    public static void clearSharedPreferences(Context context) {
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        sp.edit().clear().apply();
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
        editText.requestFocus();
    }

    public static boolean isValidEmail(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void toast(Context context, String text) {

        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public static boolean isValidUserName(String username, Context context) {
        String expression = "^[0-9a-zA-Z]+$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static void underlineWidgetWithText(TextView widget, String text) {
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        widget.setText(content);
    }

    public static String getParams(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        boolean firstIterate = true;
        for (String key : params.keySet()) {
            stringBuilder.append(firstIterate ? "?" : "&").append(key).append("=").append(URLEncoder.encode(params.get(key), "UTF-8"));
            firstIterate = false;
        }
        return stringBuilder.toString();
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static boolean timeDiff(String startDate, String endDate) {
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        Date date1 = null, date2 = null;

        try {
            date1 = simpleDateFormat.parse(startDate);
            date2 = simpleDateFormat.parse(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //milliseconds
        long different = date2.getTime() - date1.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if (elapsedDays > 0) {
            return true;
        } else if (elapsedHours > 0) {
            return true;
        } else return elapsedMinutes > 30;

    }

    public static String getData(long unixTime, String formate) {

        long unixSeconds = unixTime;
        Date date = new Date(unixSeconds);
        SimpleDateFormat sdf = new SimpleDateFormat(formate);
        String formattedDate = sdf.format(date);
        return formattedDate;
    }


    public static File getFileFromAssets(Context context, String filePath, String fileName) {
        InputStream is = null;
        File outfile = null;
        try {
            is = context.getAssets().open(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outfile = new File(context.getFilesDir(), fileName);
            //outfile.getParentFile().mkdirs();
            outfile.createNewFile();
            if (is == null)
                throw new RuntimeException("Stream is null");
            else {
                FileOutputStream out = new FileOutputStream(outfile);
                byte buf[] = new byte[1028];
                do {
                    int numRead = is.read(buf);
                    if (numRead <= 0)
                        break;
                    out.write(buf, 0, numRead);
                } while (true);

                is.close();
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return outfile;
    }

    public static String getStringFromAssetsFile(Context context, String filePath) {
        InputStream is = null;
        try {
            is = context.getAssets().open(filePath);
            return convertStreamToString(is);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }


    private static File createFileFromInputStream(InputStream inputStream, String fileName) {

        try {
            File f = new File(fileName);
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        } catch (IOException e) {
            //Logging exception
        }

        return null;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(String filePath) {
        try {
            File fl = new File(filePath);
            FileInputStream fin = new FileInputStream(fl);
            String ret = convertStreamToString(fin);
            //Make sure you close all streams.
            fin.close();
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }


    public static File unzip(Context context, File file, String destinationFolderName) {
        File unZipFolder = null;
        String name = null;
        try {
            FileInputStream fin = new FileInputStream(file.getPath());
            ZipInputStream zin = new ZipInputStream(fin);
            ZipEntry ze = null;
            /*unZipFolder = new File(context.getFilesDir(), destinationFolderName);
            if (!unZipFolder.exists()) {
                unZipFolder.mkdirs();
            }*/
            while ((ze = zin.getNextEntry()) != null) {
                // Log.v("Decompress", "Unzipping " + ze.getName());
                if (ze.isDirectory()) {
                    dirChecker(context, ze.getName());
                } else {
                    name = ze.getName();
                    FileOutputStream fout = new FileOutputStream(context.getFilesDir().getPath() + "/" + ze.getName());
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = zin.read(bytes)) >= 0) {
                        fout.write(bytes, 0, length);
                    }

                    zin.closeEntry();
                    fout.close();
                }
            }
            zin.close();
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Decompress", "unzip", e);
        }

            /* File f[] = unZipFolder.listFiles();
        for (int i = 0; i <f.length ; i++) {
           Log.e("UnZipFile",f[i].getPath());
           String  jsonString = Utilities.getStringFromFile(f[i].getPath());
            Log.e("String",jsonString);

        }*/


        return unZipFolder;
    }

    /*public static void dirChecker(Context context) {
        File f = new File(context.getFilesDir(), "all_json.json");
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }*/


    public static String md5(String encTarget) {
        MessageDigest mdEnc = null;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception while encrypting to md5");
            e.printStackTrace();
        } // Encryption algorithm
        mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
        while (md5.length() < 32) {
            md5 = "0" + md5;
        }
        return md5;
    }

    public static void dirChecker(Context context, String dir) {
        File f = new File(context.getFilesDir(), dir);

        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }

    public static int getResourceIdByName(Activity activity, String isoCode) {
        try {
            int id = activity.getResources().getIdentifier(isoCode, "drawable", activity.getPackageName());
            return id;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Drawable getDrawableByName(Activity activity, String name) {
        try {
            Resources resources = activity.getResources();
            int id = activity.getResources().getIdentifier(name, "drawable", activity.getPackageName());
            return resources.getDrawable(id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c.getTime());
    }


    public static String getAppVersion(Context context) {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info.versionName;
    }





    public static int getWindowHeight(Activity activity) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getWindowWidth(Activity activity) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static String formatDouble(double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value);

    }


    public static String getImagesFromInternalStorage(Activity activity, String path, String fileExtension) {
        File file = new File(activity.getFilesDir(), path + fileExtension);
        if (file.exists()) {
            return file.getAbsolutePath();
        } else {
            return null;
        }
    }

    public static String getDirPath(Context context) {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + context.getPackageName() + "/";
        //return context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + context.getPackageName() + "/";
    }

    public static boolean hasPermission(Activity activity, String permission) {
        // Assume thisActivity is the current activity
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void requestPermissions(final AppCompatActivity activity, final int requestCode, final String[] permissions, final String[] explainations) {
        boolean isDialogOpened = false;
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(activity, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        permissions[i])) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
                    mBuilder.setTitle("Permission Required")
                            .setMessage(explainations[i])
                            .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity,
                                            permissions,
                                            requestCode);
                                }
                            })
                            .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .create()
                            .show();
                } else {
                    if (!isDialogOpened) {
                        ActivityCompat.requestPermissions(activity,
                                permissions,
                                requestCode);
                        isDialogOpened = true;
                    }
                }
            }
        }
    }

    /**
     * @param activity     the calling {@link AppCompatActivity}
     * @param permission   from {@link android.Manifest.permission}
     * @param requestCode  The code for callback method's result
     * @param explaination Explanation for why the app needs this permission
     */
    public static void requestPermission(final AppCompatActivity activity, final int requestCode, final String permission, final String explaination) {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity,
                permission)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
                mBuilder.setTitle("Permission Required")
                        .setMessage(explaination)
                        .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity,
                                        new String[]{permission},
                                        requestCode);
                            }
                        })
                        .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(activity, "Permission Denied", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission},
                        requestCode);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public static void showRateDialog(Context context){
        SharedPreferences prefs = context.getSharedPreferences("apprater", 0);
        SharedPreferences.Editor editor = prefs.edit();
        AppRater.showRateDialog(context, editor);
    }

    public static void sendEmail(FragmentActivity context ,String[]recipients, String subject){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.setType("text/html");
        context.startActivity(Intent.createChooser(intent, "Send mail"));
    }

    public static void redirect(FragmentActivity context ,String link){
        Intent privacy = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        context.startActivity(privacy);
    }


    public static void getHashKey(FragmentActivity context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY_KEY_HASH:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void shareApp(FragmentActivity context,String packageName){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id="+packageName);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

}


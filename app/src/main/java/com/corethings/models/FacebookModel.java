package com.corethings.models;

import android.content.Context;

import com.corethings.common.Utilities;

import java.util.ArrayList;


/**
 * Created by ammar on 6/29/16.
 */
public class FacebookModel {
    static FacebookModel instance;

    public static FacebookModel getInstance(Context context) {
        if (instance == null) {
            return instance = Utilities.getObjectFromGSON(Utilities.getSaveData(context, Utilities.USER_OBJECT), FacebookModel.class);
        }
        return instance;
    }

    public static void setInstance(Context context,FacebookModel instance) {
        FacebookModel.instance = instance;
        Utilities.saveData(context, Utilities.USER_OBJECT, Utilities.getJSON(instance));

    }

    String id, name, email,cover;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}

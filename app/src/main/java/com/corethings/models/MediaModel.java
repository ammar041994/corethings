package com.corethings.models;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by ammar on 7/4/16.
 */
public class MediaModel {
    Uri uriImage;
    Bitmap image, imageLarge;
    boolean isTwitter = false;
    String id;

    public MediaModel(Uri uriImage, Bitmap image, Bitmap imageLarge, boolean isTwitter, String id) {
        this.uriImage = uriImage;
        this.image = image;
        this.imageLarge = imageLarge;
        this.isTwitter = isTwitter;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Uri getUriImage() {
        return uriImage;
    }

    public void setUriImage(Uri uriImage) {
        this.uriImage = uriImage;
    }

    public boolean isTwitter() {
        return isTwitter;
    }

    public void setTwitter(boolean twitter) {
        isTwitter = twitter;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Bitmap getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(Bitmap imageLarge) {
        this.imageLarge = imageLarge;
    }
}
